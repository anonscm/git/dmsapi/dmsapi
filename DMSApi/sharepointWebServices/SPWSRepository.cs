﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Xml;

using System.Xml.Linq;

namespace de.tarent.dms.dmsapi.spWebServices
{
    /// <summary>
    /// SPRepository is a de.tarent.sharepoint.spapi.ISPRepository implementation for MOSS2k7
    /// </summary>
    public class SPWSRepository : DMSRepository
    {
        private Dictionary<String, String> copyClientKeyMappings;

        private SPWSThreadSafeWebServiceManager wsManager;

        /// <summary>
        /// WebsSoapClient of the this repository
        /// </summary>
        internal SPWSWebsService.WebsSoapClient websClient { get { return this.wsManager.getWebsClient(this.url); } }

        /// <summary>
        /// ListsSoapClient of the this repository
        /// </summary>
        internal SPWSListsService.ListsSoapClient listsClient { get { return this.wsManager.getListsClient(this.url); } }

        /// <summary>
        /// CopySoapClient of the this repository
        /// </summary>
        internal SPWSCopyService.CopySoapClient copyClient { get { return this.wsManager.getCopyClient(this.url); } }

        /// <inheritdoc />
        public String title { get; internal set; }

        /// <inheritdoc />
        public String url { get; internal set; }

        /// <inheritdoc />
        public List<DMSLibrary> libraries { get { return this.getAllLibraries(); } }

        /// <summary>
        /// Constructor for SPRepository
        /// </summary>
        internal SPWSRepository(SPWSThreadSafeWebServiceManager wsManager, String title, String url, Dictionary<String, String> copyClientKeyMappings)
        {
            this.wsManager = wsManager;
            this.title = title;
            this.url = url;
            this.copyClientKeyMappings = copyClientKeyMappings;
        }

        /// <summary>
        /// return all libs of this repository
        /// </summary>
        internal List<DMSLibrary> getAllLibraries()
        {
            XElement myLists;
            try
            {
                //todo: is this cast save? just inserted it to make this build
                myLists = this.listsClient.GetListCollection();
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }

            if (myLists == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "the server response was unexpected", new Exception());

            List<DMSLibrary> libs = new List<DMSLibrary>();

            //todo: debug this if it works right
            foreach (XElement list in myLists.Elements())
            {
                if (list.Attribute("Title") != null && list.Attribute("ID") != null)
                {
                    libs.Add(new SPWSLibrary(this, list.Attribute("Title").Value, list.Attribute("ID").Value, this.copyClientKeyMappings));
                }
            }

            return libs;
        }

        /// <inheritdoc />
        public DMSLibrary getLibraryByTitle(String title)
        {
            //return (from lib in this.libraries
            //        where lib.title.Equals(title)
            //        select lib).Single<ISPLibrary>();
            return this.libraries.Single(lib => lib.title.Equals(title));
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj is SPWSRepository)
            {
                SPWSRepository otherRep = (SPWSRepository)obj;
                return String.Equals(this.url, otherRep.url) && String.Equals(this.title, otherRep.title);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
