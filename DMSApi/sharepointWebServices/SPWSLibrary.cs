﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Xml;
using System.Xml.Linq;
using de.tarent.dms.dmsapi.utils;

namespace de.tarent.dms.dmsapi.spWebServices
{
    /// <summary>
    /// SPLibrary is a de.tarent.sharepoint.spapi.ISPLibrary implementation for MOSS2k7
    /// </summary>
    public class SPWSLibrary : DMSLibrary
    {
        private Dictionary<String, String> copyClientKeyMappings;

        /// <inheritdoc />
        public DMSRepository repository { get { return spRepository; } }
        internal SPWSRepository spRepository { get; set; }

        /// <inheritdoc />
        public String title { get; set; }

        /// <inheritdoc />
        public String id { get; set; }

        /// <inheritdoc />
        public List<DMSItem> items { get { return this.getAllItems(null); } }

        /// <inheritdoc />
        public String CHECKINTYPE_MINOR { get { return "0"; } }

        /// <inheritdoc />
        public String CHECKINTYPE_MAJOR { get { return "1"; } }

        /// <inheritdoc />
        public String CHECKINTYPE_OVERWRITE { get { return "2"; } }

        /// <summary>
        /// Constructor for SPLibrary
        /// </summary>
        internal SPWSLibrary(SPWSRepository spRepository, String title, String id, Dictionary<String, String> copyClientKeyMappings)
        {
            this.spRepository = spRepository;
            this.title = title;
            this.id = id;
            this.copyClientKeyMappings = copyClientKeyMappings;
        }

        /// <summary>
        /// Get all items for the given query or all, if query is null.
        /// </summary>
        private List<DMSItem> getAllItems(XElement query)
        {
            SPWSListsService.ListsSoapClient listsClient = this.spRepository.listsClient;

            XElement xmlItems;
            try
            {
                xmlItems = listsClient.GetListItems(this.id, null, query, null, null, null, null);
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }

            if (xmlItems == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "the server response was unexpected", new Exception());

            List<DMSItem> itemslist = new List<DMSItem>();

            foreach (XElement node in xmlItems.Elements())
            {
                //todo: debug this if it works right
                if (node.HasElements){
                    foreach (XElement item in node.Elements())
                    {
                        if (item.HasAttributes)
                        {
                            AdvDictionary<String, String> metadata = new AdvDictionary<String, String>();
                            foreach (XAttribute att in item.Attributes())
                            {
                                metadata.Add(att.Name.ToString().Replace("ows_", ""), att.Value);
                            }
                            itemslist.Add(new SPWSItem(this, metadata));
                        }
                    }
                }
            }

            return itemslist;
        }

        /// <inheritdoc />
        public DMSItem create(AdvDictionary<String, String> itemMetadata)
        {
            SPWSListsService.ListsSoapClient lists = this.spRepository.listsClient;

            String jobs = "<Method ID=\"1\" Cmd=\"New\"><Field Name=\"ID\">New</Field>\n";
            foreach (KeyValuePair<String, String> fav in itemMetadata)
            {
                jobs += "<Field Name=\"" + fav.Key + "\"><![CDATA[" + fav.Value + "]]></Field>\n";
            }
            jobs += "</Method>\n";

            //todo: debug this due to major changes
            XDocument xmlDoc = new XDocument();
            XElement xmlElementBatch = new XElement("Batch");
            xmlDoc.Add(xmlElementBatch);

            xmlElementBatch.Add(new XAttribute("OnError", "Continue"));

            xmlElementBatch.Value = jobs;

            XElement xmlElementRequest;
            try
            {
                xmlElementRequest = lists.UpdateListItems(this.id, xmlElementBatch);
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }

            if (xmlElementRequest == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "server did not give a result");

            /*
             * todo: make this compile again
            if (Convert.ToInt32(xmlElementRequest.GetElementsByTagName("ErrorCode").Item(0).InnerXml, 16) != 0)
            {
                throw new SPException(SPException.UNEXPECTED_ERROR,
                    xmlElementRequest.GetElementsByTagName("ErrorCode").Item(0).InnerXml,
                    new Exception(xmlElementRequest.InnerXml));
            }
             * */
            
            //todo: value? was outerxml...
            XmlTextReader xr = new XmlTextReader(xmlElementRequest.Value, XmlNodeType.Element, null);
            while (xr.Read())
            {
                if (xr.ReadToFollowing("z:row"))
                {
                    String owsID = xr["ows_ID"];
                    if (owsID != null)
                    {
                        return this.items.Single(item => item.id.Equals(owsID));
                    }
                }
            }
            throw new DMSException(DMSException.UNEXPECTED_ERROR, "server did not give a valid ows_ID inside the result");
        }

        /// <inheritdoc />
        public DMSItem create(String itemFilename, AdvDictionary<String, String> itemMetadata, String pathToFile)
        {
            // create information
            SPWSCopyService.FieldInformation[] fieldInformation = new SPWSCopyService.FieldInformation[itemMetadata.Count + 1];
            
            // set filename
            fieldInformation[0] = new SPWSCopyService.FieldInformation();
            fieldInformation[0].DisplayName = "LinkFilename";
            fieldInformation[0].Type = SPWSCopyService.FieldType.Text;
            fieldInformation[0].Value = itemFilename;

            // set metadata
            int index = 1;
            foreach (KeyValuePair<String, String> kvp in itemMetadata)
            {
                String mappedKey;
                if (this.copyClientKeyMappings.TryGetValue(kvp.Key, out mappedKey))
                {
                    SPWSCopyService.FieldInformation field = new SPWSCopyService.FieldInformation();
                    field.DisplayName = mappedKey;
                    field.Type = SPWSCopyService.FieldType.Text;
                    field.Value = kvp.Value;
                    fieldInformation[index++] = field;
                }
                else
                {
                    SPWSCopyService.FieldInformation field = new SPWSCopyService.FieldInformation();
                    field.DisplayName = kvp.Key;
                    field.Type = SPWSCopyService.FieldType.Text;
                    field.Value = kvp.Value;
                    fieldInformation[index++] = field;
                }
            }

            // create
            _upload(itemFilename, FileUtils.fileToByte(pathToFile), fieldInformation);

            // return new item
            return this.items.Single(newItem => itemFilename.Equals(newItem.filename));
        }

        /// <inheritdoc />
        public void delete(DMSItem iSPItem)
        {
            SPWSItem item = checkGetSPItem(iSPItem);

            SPWSListsService.ListsSoapClient lists = this.spRepository.listsClient;


            //todo: debug this due to severe changes
            XDocument xmlDoc = new XDocument();
            XElement xmlElementBatch = new XElement("Batch");
            xmlDoc.Add(xmlElementBatch);

            xmlElementBatch.Add(new XAttribute("OnError", "Continue"));

            // check if file from sp-libaray
            if (iSPItem.filename != null)
            {
                //todo: value? was innerxml
                xmlElementBatch.Value = "<Method ID=\"1\" Cmd=\"Delete\">" +
                    "<Field Name=\"ID\">" + item.id + "</Field>" +
                    "<Field Name=\"FileRef\">" + getItemServerUrl(item) + "</Field>" +
                    "</Method>";
            }
            // or from sp-list
            else
            {
                xmlElementBatch.Value = "<Method ID=\"1\" Cmd=\"Delete\">" +
                    "<Field Name=\"ID\">" + item.id + "</Field></Method>";
            }

            XElement xmlElementRequest;
            try
            {
                xmlElementRequest = lists.UpdateListItems(this.id, xmlElementBatch);
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }

            if (xmlElementRequest == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "server did not give a result", new Exception());

            /*
            if (Convert.ToInt32(xmlElementRequest.GetElementsByTagName("ErrorCode").Item(0).InnerXml, 16) != 0)
            {
                throw new SPException(SPException.UNEXPECTED_ERROR,
                    xmlElementRequest.GetElementsByTagName("ErrorCode").Item(0).InnerXml,
                    new Exception(xmlElementRequest.InnerXml));
            }*/
        }

        /// <inheritdoc />
        public void download(DMSItem iSPItem, string toLocalPathFile)
        {   
            FileStream fs = null;
            try
            {
                fs = new FileStream(toLocalPathFile, FileMode.Create, FileAccess.ReadWrite);

                BinaryWriter bw = null;
                try
                {
                    bw = new BinaryWriter(fs);
                    bw.Write(_download(iSPItem));
                }
                finally
                {
                    if (bw != null) bw.Close();
                }
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        /// <inheritdoc />
        public Stream download(DMSItem iSPItem)
        {
            return new MemoryStream(_download(iSPItem));
        }

        private byte[] _download(DMSItem iSPItem)
        {
            SPWSItem spItem = checkGetSPItem(iSPItem);

            SPWSCopyService.FieldInformation myFieldInfo = new SPWSCopyService.FieldInformation();
            SPWSCopyService.FieldInformation[] myFieldInfoArray = { myFieldInfo };

            SPWSCopyService.CopySoapClient copyClient = this.spRepository.copyClient;

            byte[] binaryData;
            try
            {
                copyClient.GetItem(getItemServerUrl(spItem), out myFieldInfoArray, out binaryData);
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }

            if (binaryData == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "server did not fill the files byte-array (" + getItemServerUrl(spItem) + ")", new Exception());

            return binaryData;
        }

        /// <inheritdoc />
        public bool checkOut(DMSItem iSPItem)
        {
            SPWSItem item = checkGetSPItem(iSPItem);

            String itemUrl = getItemServerUrl(item);

            SPWSListsService.ListsSoapClient listsService = this.spRepository.listsClient;
            try
            {
                //the "false" parameter could also be a parameter of this method
                //it just was not needed until now
                //we had to change it from true to false after a ms security update:
                //http://www.microsoft.com/germany/technet/sicherheit/bulletins/ms10-039.mspx
                //the fact its working with always set to false could be another bug
                //fixed in the future
                return listsService.CheckOutFile(itemUrl, "false", item.lastModified);
            }           
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }
        }

        /// <inheritdoc />
        public void upload(DMSItem iSPItem, string fromLocalPathFile, bool alsoUpdateMetadata)
        {
            _upload(iSPItem, FileUtils.fileToByte(fromLocalPathFile), alsoUpdateMetadata);    
        }

        /// <inheritdoc />
        public void upload(DMSItem iSPItem, Stream content, bool alsoUpdateMetadata)
        {
            _upload(iSPItem, FileUtils.streamToByte(content), alsoUpdateMetadata);
        }

        private void _upload(DMSItem iSPItem, byte[] content, bool alsoUpdateMetadata)
        {
            SPWSItem spItem = checkGetSPItem(iSPItem);

            // create information
            SPWSCopyService.FieldInformation[] fieldInformation;
            if (alsoUpdateMetadata)
            {
                fieldInformation = new SPWSCopyService.FieldInformation[spItem.metadata.Count];
                int index = 0;
                foreach (KeyValuePair<String, String> kvp in spItem.metadata)
                {
                    String mappedKey;
                    if (this.copyClientKeyMappings.TryGetValue(kvp.Key, out mappedKey))
                    {
                        SPWSCopyService.FieldInformation field = new SPWSCopyService.FieldInformation();
                        field.DisplayName = mappedKey;
                        field.Type = SPWSCopyService.FieldType.Text;
                        field.Value = kvp.Value;
                        fieldInformation[index++] = field;
                    }
                    else
                    {
                        SPWSCopyService.FieldInformation field = new SPWSCopyService.FieldInformation();
                        field.DisplayName = kvp.Key;
                        field.Type = SPWSCopyService.FieldType.Text;
                        field.Value = kvp.Value;
                        fieldInformation[index++] = field;
                    }
                }
            }
            else
            {
                fieldInformation = new SPWSCopyService.FieldInformation[0];
            }

            _upload(spItem.filename, content, fieldInformation);
        }

        /// <inheritdoc />
        public bool checkIn(DMSItem iSPItem, String comment, String checkInType)
        {
            SPWSItem item = checkGetSPItem(iSPItem);

            String itemUrl = getItemServerUrl(item);

            SPWSListsService.ListsSoapClient lists = this.spRepository.listsClient;
            try
            {
                return lists.CheckInFile(itemUrl, comment, checkInType);
            }
            catch (EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }
        }

        /// <inheritdoc />
        public void undoCheckOut(DMSItem iSPItem)
        {
            SPWSItem item = checkGetSPItem(iSPItem);

            String itemUrl = getItemServerUrl(item);

            SPWSListsService.ListsSoapClient lists = this.spRepository.listsClient;
            try
            {
                if (!lists.UndoCheckOut(itemUrl))
                    throw new DMSException(DMSException.UNEXPECTED_ERROR, "undoCheckOut(): could not check out");
            }
            catch (EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }
        }

        /// <inheritdoc />
        public void update(DMSItem iSPItem)
        {
            SPWSItem item = checkGetSPItem(iSPItem);

            if (item.originalMetadata.Equals(item.metadata))
            {
                return;
            }
            else
            {
                SPWSListsService.ListsSoapClient lists = this.spRepository.listsClient;

                String jobs = "<Method ID=\"1\" Cmd=\"Update\"><Field Name=\"ID\">" + item.id + "</Field>\n";
                foreach (KeyValuePair<String, String> fav in item.metadata)
                {
                    String orgValue = null;
                    item.originalMetadata.TryGetValue(fav.Key, out orgValue);
                    if (!fav.Value.Equals(orgValue))
                    {
                        jobs += "<Field Name=\"" + fav.Key + "\"><![CDATA[" + fav.Value + "]]></Field>\n";
                    }
                }
                jobs += "</Method>\n";

                //todo: does this still work? severe changes
                XDocument xmlDoc = new XDocument();
                XElement xmlElementBatch = new XElement("Batch");
                xmlDoc.Add(xmlElementBatch);

                xmlElementBatch.Add(new XAttribute("OnError", "Continue"));

                //value? was innerxml
                xmlElementBatch.Value = jobs;

                XElement xmlElementRequest;
                try
                {
                    xmlElementRequest = lists.UpdateListItems(item.spLibrary.id, xmlElementBatch);
                }
                catch (System.ServiceModel.EndpointNotFoundException enfe)
                {
                    throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
                }
                catch (System.ServiceModel.Security.MessageSecurityException mse)
                {
                    if (mse.InnerException.Message.Contains("401"))
                    {
                        throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                    }
                    throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
                }
                catch (Exception e)
                {
                    throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
                }

                if (xmlElementRequest == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "server did not give a result", new Exception());

                /*
                 * todo: make this work
                if (Convert.ToInt32(xmlElementRequest.GetElementsByTagName("ErrorCode").Item(0).InnerXml, 16) != 0)
                {
                    throw new SPException(SPException.UNEXPECTED_ERROR, 
                        xmlElementRequest.GetElementsByTagName("ErrorCode").Item(0).InnerXml, 
                        new Exception(xmlElementRequest.InnerXml));
                }*/
            }
        }

        /// <inheritdoc />
        public DMSItem getByID(String itemId)
        {
            XElement xmlQuery = new XElement("Query");

            //was innerxml
            xmlQuery.Value = "<Where>" +
                                "<Eq>" +
                                "<FieldRef Name='ID' />" +
                                "<Value Type='Text'><![CDATA[" + itemId + "]]></Value>" +
                                "</Eq>" +
                                "</Where>";

            return getAllItems(xmlQuery).SingleOrDefault();
        }

        /// <inheritdoc />
        public DMSItem getByFilename(String itemFilename)
        {
            // doesn't work :(
            //
            //XmlElement xmlQuery = new XmlDocument().CreateElement("Query");
            //xmlQuery.InnerXml = "<Where>" +
            //                    "<Eq>" +
            //                    "<FieldRef Name='LinkFilename' />" +
            //                    "<Value Type='Text'><![CDATA[" + itemFilename + "]]></Value>" +
            //                    "</Eq>" +
            //                    "</Where>";

            return getAllItems(null).SingleOrDefault(item => itemFilename.Equals(item.filename));
        }

        /// <summary>
        /// methed for uploading / creation of spItems
        /// </summary>
        private void _upload(String itemFilename, byte[] fileContent, SPWSCopyService.FieldInformation[] fieldInformation)
        {
            SPWSCopyService.CopySoapClient copy = this.spRepository.copyClient;

            if (fileContent == null) throw new DMSException(DMSException.UNEXPECTED_ERROR, "file could not be uploaded", new Exception());

            string copySource = "http://uploadedByTarentMultiEdit";
            string[] copyDest = { getItemServerUrl(itemFilename) };

            SPWSCopyService.CopyResult myCopyResult1 = new SPWSCopyService.CopyResult();
            SPWSCopyService.CopyResult[] myCopyResultArray = { myCopyResult1 };

            uint myCopyUint;
            try
            {
                myCopyUint = copy.CopyIntoItems(copySource, copyDest, fieldInformation, fileContent, out myCopyResultArray);
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }

            bool success = false;
            if (myCopyUint == 0)
            {
                int idx = 0;
                foreach (SPWSCopyService.CopyResult myCopyResult in myCopyResultArray)
                {
                    string opString = (idx + 1).ToString();
                    if (myCopyResultArray[idx].ErrorMessage == null)
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }
                }
            }

            if (!success) throw new DMSException(DMSException.UNEXPECTED_ERROR, "server result indicated a problem", new Exception(myCopyResultArray[1].ErrorMessage));
        }

        private SPWSItem checkGetSPItem(DMSItem item)
        {
            if (!(item is SPWSItem))
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "ISPItem is not a moss2k7.SPItem, item=" + item);
            }
            if (! this.Equals(item.library))
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "ISPItem is not type of these libaray" +
                    ", this=" + this +
                    ", item=" + item +
                    ", item.library=" + item.library);
            }
            return (SPWSItem)item;
        }

        /// <summary>
        /// Return the server url for the given item.
        /// </summary>
        private String getItemServerUrl(SPWSItem item)
        {
            if (item.filename == null)
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "create server url for item isn't possible, maybe a list item? " + item);
            }
            return this.spRepository.url + "/" + this.title + "/" + item.filename;
        }

        /// <summary>
        /// Return the server url for the given itemFilename.
        /// </summary>
        private String getItemServerUrl(String itemFilename)
        {
            if (itemFilename == null)
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "create server url for itemFilename isn't possible, maybe an list item? " + itemFilename);
            }
            return this.spRepository.url + "/" + this.title + "/" + itemFilename;
        }

       
        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj is SPWSLibrary)
            {
                SPWSLibrary otherLib = (SPWSLibrary)obj;
                return String.Equals(this.id, otherLib.id) && String.Equals(this.title, otherLib.title) && this.repository.Equals(otherLib.repository);
            }
            return false;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            int hashCode = 7;
            hashCode = hashCode * 31 + (id != null ? id.GetHashCode() : 0);
            hashCode = hashCode * 31 + (title != null ? title.GetHashCode() : 0);
            return hashCode;
        }

        /// <inheritdoc />
        public override String ToString()
        {
            return GetType().Name + "["
                + "title=" + title
                + ", id=" + id
                + "]";
        }
    }
}
