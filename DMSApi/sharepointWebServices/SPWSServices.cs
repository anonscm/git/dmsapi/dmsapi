﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Configuration;
using System.ServiceModel.Configuration;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml;
using System.Collections;
using System.IO;
using System.Net;
using System.Xml.Linq;

namespace de.tarent.dms.dmsapi.spWebServices
{
    /// <summary>
    /// SPServices is a de.tarent.sharepoint.spapi.ISPServices implementation for MOSS2k7
    /// </summary>
    public class SPWSServices : DMSServices
    {
        private Dictionary<String, String> copyClientKeyMappings;

        private SPWSThreadSafeWebServiceManager wsManager;

        /// <summary>
        /// Constructor for SPServices
        /// </summary>
        /// <param name="copyClientKeyMappings">Dictionary contains key-mapping for client.</param>
        public SPWSServices(Dictionary<String, String> copyClientKeyMappings)
        {
            // set copyClientKeyMappings
            this.copyClientKeyMappings = copyClientKeyMappings != null ? copyClientKeyMappings : new Dictionary<String, String>();

            // create a default binding for the webservice clients
            BasicHttpBinding httpBinding = new BasicHttpBinding();
            httpBinding.Name = "WebsSoap";
           
            httpBinding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            httpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm; // Windows

            // create SPWebServiceManager
            this.wsManager = new SPWSThreadSafeWebServiceManager(httpBinding);
        }

        /// <summary>
        /// Constructor for SPServices
        /// </summary>
        public SPWSServices(BasicHttpBinding httpBinding, Dictionary<String, String> copyClientKeyMappings)
        {
            // set copyClientKeyMappings
            this.copyClientKeyMappings = copyClientKeyMappings != null ? copyClientKeyMappings : new Dictionary<String, String>();

            // create SPWebServiceManager
            this.wsManager = new SPWSThreadSafeWebServiceManager(httpBinding);
        }

        /// <inheritdoc />
        public DMSRepository getRepository(String url)
        {
            SPWSWebsService.WebsSoapClient websClient;

            XElement getWebsResponse;
            try
            {
                websClient = this.wsManager.getWebsClient(url);
                getWebsResponse = websClient.GetWeb(url);

                //todo: debug this if changes work
                if (getWebsResponse != null)
                {
                    if (getWebsResponse.Attribute("Title") != null && getWebsResponse.Attribute("Url") != null)
                    {
                        return new SPWSRepository(
                            this.wsManager,
                            getWebsResponse.Attribute("Title").Value,
                            getWebsResponse.Attribute("Url").Value,
                            this.copyClientKeyMappings
                        );
                    }
                }
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "the servers result could not be interpreted", new Exception());
            }
            catch (System.ServiceModel.EndpointNotFoundException enfe)
            {
                throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, enfe.Message, enfe as Exception);
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.InnerException.Message.Contains("401"))
                {
                    throw new DMSException(DMSException.NOT_AUTHORIZED, mse.Message, mse as Exception);
                }
                throw new DMSException(DMSException.UNEXPECTED_ERROR, mse.Message, mse as Exception);
            }
            catch (Exception e)
            {
                throw new DMSException(DMSException.UNEXPECTED_ERROR, e.Message, e);
            }
        }
    }
}
