﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Threading;

namespace de.tarent.dms.dmsapi.spWebServices
{
    /// <summary>
    /// The ThreadSafeWebServiceManager manage all used WebServices in a thread safe way.
    /// </summary>
    public class SPWSThreadSafeWebServiceManager
    {
        private BasicHttpBinding httpBinding;

        public SPWSThreadSafeWebServiceManager(BasicHttpBinding httpBinding) { this.httpBinding = httpBinding; }

        internal SPWSWebsService.WebsSoapClient getWebsClient(String url)
        {
            String serviceUrl = url + "/_vti_bin/Webs.asmx";
            LocalDataStoreSlot namedDataSlot = Thread.GetNamedDataSlot(GetType().Name + serviceUrl);
            SPWSWebsService.WebsSoapClient websClient = (SPWSWebsService.WebsSoapClient)Thread.GetData(namedDataSlot);
            if (websClient == null)
            {
                websClient = createWebsClient(serviceUrl);
                Thread.SetData(namedDataSlot, websClient);
            }
            return websClient;
        }

        private SPWSWebsService.WebsSoapClient createWebsClient(String serviceUrl)
        {
            System.ServiceModel.EndpointAddress remoteAddress = new EndpointAddress(serviceUrl);
            SPWSWebsService.WebsSoapClient websClient = new SPWSWebsService.WebsSoapClient(httpBinding, remoteAddress);
            websClient.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return websClient;
        }

        internal SPWSListsService.ListsSoapClient getListsClient(String url)
        {
            String serviceUrl = url + "/_vti_bin/Lists.asmx";
            LocalDataStoreSlot namedDataSlot = Thread.GetNamedDataSlot(GetType().Name + serviceUrl);
            SPWSListsService.ListsSoapClient listsClient = (SPWSListsService.ListsSoapClient)Thread.GetData(namedDataSlot);
            if (listsClient == null)
            {
                listsClient = createListsClient(serviceUrl);
                Thread.SetData(namedDataSlot, listsClient);
            }
            return listsClient;
        }

        private SPWSListsService.ListsSoapClient createListsClient(String serviceUrl)
        {
            System.ServiceModel.EndpointAddress remoteAddress = new EndpointAddress(serviceUrl);
            SPWSListsService.ListsSoapClient listClient = new SPWSListsService.ListsSoapClient(httpBinding, remoteAddress);
            listClient.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return listClient;
        }

        internal SPWSCopyService.CopySoapClient getCopyClient(String url)
        {
            String serviceUrl = url + "/_vti_bin/Copy.asmx";
            LocalDataStoreSlot namedDataSlot = Thread.GetNamedDataSlot(GetType().Name + serviceUrl);
            SPWSCopyService.CopySoapClient copyClient = (SPWSCopyService.CopySoapClient)Thread.GetData(namedDataSlot);
            if (copyClient == null)
            {
                copyClient = createCopyClient(serviceUrl);
                Thread.SetData(namedDataSlot, copyClient);
            }
            return copyClient;
        }

        private SPWSCopyService.CopySoapClient createCopyClient(String serviceUrl)
        {
            System.ServiceModel.EndpointAddress remoteAddress = new EndpointAddress(serviceUrl);
            SPWSCopyService.CopySoapClient copyClient = new SPWSCopyService.CopySoapClient(httpBinding, remoteAddress);
            copyClient.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            return copyClient;
        }
    }
}
