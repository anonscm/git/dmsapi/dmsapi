﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using de.tarent.dms.dmsapi.utils;
using System.IO;

namespace de.tarent.dms.dmsapi
{
    /// <summary>
    /// ISPItem abstract a list / library inside a sharepoint site
    /// </summary>
    public interface DMSLibrary
    {        
        /// <summary>
        /// ID these library
        /// </summary>
        String id { get; set; }

        /// <summary>
        /// Titel these library
        /// </summary>
        String title { get; set; }

        /// <summary>
        /// Repository these library belongs to
        /// </summary>
        DMSRepository repository { get; }

        /// <summary>
        /// List of all items in these library
        /// </summary>
        List<DMSItem> items { get; }

        /// <summary>
        /// Update the metadatas of the given item
        /// </summary>
        void update(DMSItem item);

        /// <summary>
        /// Checkin the given item.
        /// </summary>
        bool checkIn(DMSItem item, string comment, string checkInType);

        /// <summary>
        /// key for a minor version update
        /// </summary>
        String CHECKINTYPE_MINOR { get; }

        /// <summary>
        /// key for a major version update
        /// </summary>
        String CHECKINTYPE_MAJOR { get; }

        /// <summary>
        /// key for a override version update
        /// </summary>
        String CHECKINTYPE_OVERWRITE { get; }

        /// <summary>
        /// creates a new item for the given metadatas, typical called on a SP-List 
        /// </summary>
        DMSItem create(AdvDictionary<String, String> itemMetadata);

        /// <summary>
        /// creates a new item for the given metadatas and file, typical called on a SP-Libaray
        /// </summary>
        DMSItem create(String itemFilename, AdvDictionary<String, String> itemMetadata, String pathToFile);

        /// <summary>
        /// delete the given item
        /// </summary>
        void delete(DMSItem item);

        /// <summary>
        /// checkout the given item.
        /// </summary>
        bool checkOut(DMSItem item);

        /// <summary>
        /// undo the checkout of the given item.
        /// </summary>
        void undoCheckOut(DMSItem item);

        /// <summary>
        /// Download the binary datas of the given item to the given local path.
        /// </summary>
        void download(DMSItem item, string toLocalFilePath);

        /// <summary>
        /// Return a stream of the binary datas for the given item.
        /// </summary>
        Stream download(DMSItem item);

        /// <summary>
        /// Upload the binary datas from the given fromLocalPath for the given item.
        /// </summary>
        void upload(DMSItem item, string fromLocalFilePath, bool updateMetadata);

        /// <summary>
        /// Upload the data from the given Stream for the given item.
        /// </summary>
        void upload(DMSItem item, Stream content, bool updateMetadata);

        /// <summary>
        /// Get an item for a given ID or null if not found.
        /// </summary>
        DMSItem getByID(String itemId);

        /// <summary>
        /// Get an item for a given filename or null if not found.
        /// </summary>
        DMSItem getByFilename(String filename);
    }
}
