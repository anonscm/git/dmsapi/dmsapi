﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;

using Microsoft.SharePoint.Client;
using de.tarent.dms.dmsapi.utils;

namespace de.tarent.dms.dmsapi.spClientOM
{
    /// <summary>
    /// SPItem is a de.tarent.sharepoint.spapi.ISPItem implementation for spOnline
    /// </summary>
    public class SPComItem : DMSItem
    {
        /// <inheritdoc />
        public DMSLibrary library { get; set; }
        private ClientContext context;
        internal ListItem spItem;
        internal File spFile;

        /// <inheritdoc />
        public String id {
            get { return getMetadata("ID");  }
            set { setMetadata("ID", value); }
        }

        /// <inheritdoc />
        public String title
        {
            get { return getMetadata("Title"); }
            set { setMetadata("Title", value); }
        }

        /// <inheritdoc />
        public String filename
        {
            get { return spFile.Name; }
            set { throw new DMSException(DMSException.FUNCTIONAL_ERROR, "you cannot set filename!"); }
        }

        /// <inheritdoc />
        public String lastModified
        {
            get { return getMetadata("Modified"); }
            set { setMetadata("Modified", value); }
        }

        /// <inheritdoc />
        public String checkedOutBy
        {
            get { return getMetadata("CheckoutUser"); }
            set { setMetadata("CheckoutUser", value); }
        }

        /// <inheritdoc />
        public AdvDictionary<String, String> metadata { get; set; }

        internal AdvDictionary<String, String> originalMetadata;

        /// <summary>
        /// Constructor for SPItem
        /// </summary>
        public SPComItem(ListItem spItem, File spFile, DMSLibrary parentLib, ClientContext context, AdvDictionary<string, string> metadata)
        {
            this.spFile = spFile;
            this.library = parentLib;
            this.metadata = metadata;
            this.spItem = spItem;
            this.context = context;
            this.originalMetadata = new AdvDictionary<String, String>(this.metadata);
        }

        /// <summary>
        /// Constructor for SPItem
        /// </summary>
        private SPComItem(ListItem spItem, DMSLibrary parentLib, ClientContext context, AdvDictionary<string, string> metadata, AdvDictionary<string, string> originalMetadata)
        {
            this.library = parentLib;
            this.metadata = metadata;
            this.spItem = spItem;
            this.context = context;
            this.originalMetadata = originalMetadata;
        }

        /// <inheritdoc />
        public String getMetadata(String key)
        {
            String value;
            return this.metadata.TryGetValue(key, out value) ? value : null;
        }

        /// <inheritdoc />
        public void setMetadata(String key, String value)
        {
            lock (this.metadata)
            {
                if (metadata.ContainsKey(key)) { metadata.Remove(key); }
                metadata.Add(key, value);
            }
        }

        /// <inheritdoc />
        public DMSItem copy()
        {
            lock (this)
            {
                return new SPComItem(this.spItem, this.library, this.context, 
                    new AdvDictionary<String, String>(this.metadata), 
                    new AdvDictionary<String, String>(this.originalMetadata));
            }
        }

        /// <inheritdoc />
        public DMSItem reload()
        {
            return this.library.getByID(this.id);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SPComItem)) {
                return false;
            }

            SPComItem otherItem = (SPComItem) obj;

            return (this.library == otherItem.library ? true : this.library == null ? false : this.library.Equals(otherItem.library))
                && (this.metadata == otherItem.metadata ? true : this.metadata == null ? false : this.metadata.Equals(otherItem.metadata));
        }

        /// <inheritdoc />
        public override String ToString()
        {
            return GetType().Name
                + ", id: " + id
                + ", filename: " + filename
                + ", checkedOutBy: " + checkedOutBy
                + ", lastModified: " + lastModified
                + ", metadata: " + metadata
                + ", library: " + library;
        }
    }
}
