﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Wictor.Office365;
using System.Net;

namespace de.tarent.dms.dmsapi.spClientOM
{
    public class SPComRepository : DMSRepository
    {
        private ClientContext context;
        internal SPComServices services;

        public string title { get; internal set; }

        public string url { get; internal set; }

        /// <inheritdoc />
        public SPComRepository(SPComServices services, String url)
        {
            this.services = services;
            this.context = services.createNewContext(url);
            this.url = context.Url;

            try
            {
                lock (context)
                {
                    Web web = context.Web;
                    context.Load(web, w => w.Title);
                    context.ExecuteQuery();
                    this.title = web.Title;
                }
            }
            catch (System.NullReferenceException nre)
            {
                throw new DMSException(DMSException.CONFIGURATION_ERROR, "the repository may not exist or the user credentials may be wrong", nre);
            }
        }

        /// <inheritdoc />
        public List<DMSLibrary> libraries
        {
            get
            {
                List<DMSLibrary> libs = new List<DMSLibrary>();
                lock (context)
                {
                    ListCollection lists = context.Web.Lists;
                    context.Load(lists);
                    context.ExecuteQuery();
                    foreach (List list in lists)
                    {
                        libs.Add(new SPComLibrary(context, list, this));
                    }
                }
                return libs;
            }
        }

        /// <inheritdoc />
        public DMSLibrary getLibraryByTitle(String title)
        {
            //todo: optimize (linq/lambda)
            return this.libraries.Single(lib => lib.title.Equals(title));
        }
    }
}
