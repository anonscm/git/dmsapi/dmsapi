﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using de.tarent.dms.dmsapi.utils;

namespace de.tarent.dms.dmsapi.spClientOM
{
    public class SPComLibrary : DMSLibrary
    {
        public DMSRepository repository { get; private set; }
        private ClientContext context;
        private List spList;
        public string id { get; set; }
        public string title { get; set; }

        /// <inheritdoc />
        public String CHECKINTYPE_MINOR { get { return "0"; } }

        /// <inheritdoc />
        public String CHECKINTYPE_MAJOR { get { return "1"; } }

        /// <inheritdoc />
        public String CHECKINTYPE_OVERWRITE { get { return "2"; } }

        public HashSet<string> metaBlackList { get; set; }

        public SPComLibrary(ClientContext context, List spList, DMSRepository parent)
        {
            this.repository = parent;
            this.context = context;
            this.spList = spList;

            this.metaBlackList = new HashSet<string>();
            metaBlackList.Add("Modified");
            metaBlackList.Add("Last_x0020_Modified");
            metaBlackList.Add("File_x0020_Size");
            metaBlackList.Add("MetaInfo");
            metaBlackList.Add("owshiddenversion");
            metaBlackList.Add("Id");
            metaBlackList.Add("DocConcurrencyNumber");

            lock (context)
            {
                context.Load(this.spList);
                context.ExecuteQuery();
                this.title = this.spList.Title;
                this.id = this.spList.Id.ToString();
            }            
        }

        /// <inheritdoc />
        public List<DMSItem> items
        {
            get 
            {
                CamlQuery query = new CamlQuery();
                ListItemCollection spItems = spList.GetItems(new CamlQuery());
                FileCollection fileCollection = spList.RootFolder.Files;
                List<DMSItem> items = new List<DMSItem>();

                lock (context)
                {
                    context.Load(spItems);
                    context.ExecuteQuery();

                    foreach (ListItem spItem in spItems)
                    {

                        Dictionary<String, Object> fieldValues = spItem.FieldValues;
                        AdvDictionary<string, string> metadata = new AdvDictionary<string, string>();
                        foreach (KeyValuePair<String, Object> fieldValue in fieldValues)
                        {
                            string value = fieldValue.Value != null ? fieldValue.Value.ToString() : "";
                            metadata.Add(fieldValue.Key, value);
                        }
                        File file = null;

                            if(spList.BaseType.Equals(BaseType.DocumentLibrary))
                            {
                                file = spItem.File;
                                context.Load(file);
                            }
                         
                        items.Add(new SPComItem(spItem, file, this, context, metadata));
                    }
                    context.ExecuteQuery();
                }
                return items;
            }
        }

        private AdvDictionary<string, string> getChangedMetadata(SPComItem item)
        {
            AdvDictionary<string, string> changed = new AdvDictionary<string, string>();
            foreach (KeyValuePair<String, String> fav in item.metadata)
            {
                String orgValue = null;
                item.originalMetadata.TryGetValue(fav.Key, out orgValue);
                if (!fav.Value.Equals(orgValue) && !metaBlackList.Contains(fav.Key))
                {
                    changed.Add(fav.Key, fav.Value);
                }
            }
            return changed;
        }

        /// <inheritdoc />
        public void update(DMSItem dmsitem)
        {         
            SPComItem item = dmsitem as SPComItem;

            if (item.originalMetadata.Equals(item.metadata))
            {
                return;
            }
            else
            {
                lock (context)
                {
                    if (BaseType.DocumentLibrary.Equals(spList.BaseType))
                    {
                        try
                        {
                            checkOut(item);
                        }
                        catch
                        {
                            //todo: llallala
                        }
                    }

                    ListItem spItem = item.spItem;
                    AdvDictionary<string, string> changedMeta = getChangedMetadata(item);

                    foreach (KeyValuePair<String, String> fav in changedMeta)
                    {
                        spItem[fav.Key] = fav.Value;
                    }
                    spItem.Update();
                    context.ExecuteQuery();

                    if (BaseType.DocumentLibrary.Equals(spList.BaseType))
                    {
                        checkIn(item, "updated", CHECKINTYPE_OVERWRITE);
                    }
                }
            }
        }

        /// <summary>
        /// helps to translate the string used for more simple dms-apis to the com type
        /// </summary>
        /// <param name="checkInType"></param>
        /// <returns></returns>
        private CheckinType convertCheckInType(string checkInType)
        {
            if(CHECKINTYPE_MINOR.Equals(checkInType))
            {
                return CheckinType.MinorCheckIn;
            }
            if(CHECKINTYPE_OVERWRITE.Equals(checkInType))
            {
                return CheckinType.OverwriteCheckIn;
            }
            return CheckinType.MajorCheckIn;
        }

        /// <inheritdoc />
        public DMSItem create(utils.AdvDictionary<string, string> itemMetadata)
        {
            String title;
            if(!itemMetadata.TryGetValue("Title", out title))
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "a field \"Title\" needs to be set in item metadata");
            }

            ListItemCreationInformation parameters = new ListItemCreationInformation();
            parameters.LeafName = title;
            ListItem newItem = spList.AddItem(parameters);
            File file = null;

            lock (context)
            {
                foreach (KeyValuePair<string, string> kvp in itemMetadata)
                {
                    newItem[kvp.Key] = kvp.Value;
                }
                newItem.Update();

                if (BaseType.DocumentLibrary.Equals(spList.BaseType))
                {
                    file = newItem.File;
                    context.Load(file);
                }
                context.ExecuteQuery();
            }
            return new SPComItem(newItem, file, this, context, itemMetadata);
        }

        /// <inheritdoc />
        public DMSItem create(string itemFilename, utils.AdvDictionary<string, string> itemMetadata, string pathToFile)
        {
            string url = context.Url + "/" + spList.Title + "/" + itemFilename;
            byte[] content = FileUtils.fileToByte(pathToFile);
            String title;
            if(!itemMetadata.TryGetValue("Title", out title))
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "you need to set Field \"Title\" in metadata to create an item");
            }
            // upload content and set title
            File file = upload(url, content, title);

            // grab dmsitem
            DMSItem item = items.Single(nitem => title.Equals(nitem.title));
            
            // set and update meta
            item.metadata = itemMetadata;
            update(item);

            // return it
            return item;
        }

        /// <inheritdoc />
        public void delete(DMSItem item)
        {
            lock (context)
            {
                ((SPComItem)item).spItem.DeleteObject();
                context.ExecuteQuery();
            }
        }

        /// <inheritdoc />
        public bool checkOut(DMSItem item)
        {
            File file = ((SPComItem)item).spFile;
            Boolean success;
            if (file == null)
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "items with no associated file cannot be checked out");
            }

            lock (context)
            {
                try
                {
                    file.CheckOut();
                    context.ExecuteQuery();
                    success = true;
                }
                catch (ServerException)
                {
                    success = false;
                }
            }
            return success;
        }

        /// <inheritdoc />
        public void undoCheckOut(DMSItem item)
        {
            File file = ((SPComItem)item).spFile;
            if (file == null)
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "items with no associated file cannot be checked out");
            }
            lock (context)
            {
                file.UndoCheckOut();
                context.ExecuteQuery();
            }
        }

        /// <inheritdoc />
        public bool checkIn(DMSItem item, string comment, string checkInType)
        {
            File file = ((SPComItem)item).spFile;
            if (file == null)
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "items with no associated file cannot be checked in");
            }
            Boolean success;
            lock (context)
            {
                try
                {
                    file.CheckIn(comment, convertCheckInType(checkInType));
                    context.ExecuteQuery();
                    success = true;
                }
                catch (ServerException)
                {
                    success = false;
                }
            }
            return success;
        }

        /// <inheritdoc />
        public void download(DMSItem item, string toLocalFilePath)
        {
            FileUtils.StreamToFile(download(item), toLocalFilePath);
        }

        /// <inheritdoc />
        public System.IO.Stream download(DMSItem dmsitem)
        {

            //not sure whether a new context is a good idea. anyway.. NOT using a new context resulted in webdav errors from time to time

            using (ClientContext context = (repository as SPComRepository).services.createNewContext(repository.url))
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                SPComItem item = dmsitem as SPComItem;
                context.Load(item.spItem);
                context.Load(item.spFile);
                context.ExecuteQuery();
                FileInformation fileInformation = File.OpenBinaryDirect(context, item.spItem["FileRef"].ToString());
                FileUtils.CopyStream(fileInformation.Stream, stream);
                return stream;
            }
            
            
            /*
            lock (context)
            {
                context.ExecuteQuery();
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                SPComItem item = (SPComItem)dmsitem;
                context.Load(item.spItem);
                context.Load(item.spFile);
                context.ExecuteQuery();
                FileInformation fileInformation = File.OpenBinaryDirect(context, item.spItem["FileRef"].ToString());
                FileUtils.CopyStream(fileInformation.Stream, stream);
                return stream;
            }
            */
        }

        /// <summary>
        /// helper method for creation and overwrite of files
        /// </summary>
        /// <param name="path"></param>
        /// <param name="content"></param>
        private File upload(string url, byte[] content, String title)
        {
            if (title == null || title.Length < 1)
            {
                throw new DMSException(DMSException.FUNCTIONAL_ERROR, "title cannot be null");
            }

            FileCreationInformation creationInformation = new FileCreationInformation();
            creationInformation.Content = content;
            creationInformation.Overwrite = true;
            creationInformation.Url = url;
            File newFile = spList.RootFolder.Files.Add(creationInformation);
            lock (context)
            {
                context.ExecuteQuery();
                try
                {
                    newFile.CheckOut();
                    context.ExecuteQuery();
                }
                catch
                {
                    //do nothing - todo: how to find out whether its checked out or not to only execute above code when nessecary
                }
                newFile.ListItemAllFields["Title"] = title;
                newFile.ListItemAllFields.Update();
                newFile.CheckIn("updated", CheckinType.MajorCheckIn);

                context.ExecuteQuery();
            }
            return newFile;
        }

        /// <inheritdoc />
        public void upload(DMSItem item, string fromLocalFilePath, bool updateMetadata)
        {
            byte[] content = FileUtils.fileToByte(fromLocalFilePath);
            upload(item, content, updateMetadata);
        }

        /// <inheritdoc />
        public void upload(DMSItem item, System.IO.Stream stream, bool updateMetadata)
        {
            byte[] content = FileUtils.streamToByte(stream);
            upload(item, content, updateMetadata);
        }

        /// <summary>
        /// helper for update methods
        /// </summary>
        /// <param name="item"></param>
        /// <param name="content"></param>
        /// <param name="updateMetadata"></param>
        private void upload(DMSItem item, byte[] content, bool updateMetadata)
        {
            String url = context.Url + "/" + spList.Title + "/" + item.filename;
            File file = upload(url, content, item.title);
            if (updateMetadata)
            {
                DMSItem newitem = items.Single(nitem => item.title.Equals(nitem.title));

                //copy desired metadata to item because we lost it on the overwrite event
                newitem.metadata = item.metadata;

                //push it into the new item 
                update(newitem);
            }
        }

        /// <inheritdoc />
        public DMSItem getByID(string itemId)
        {
            //todo: optimize (linq/lambda)
            return this.items.Single(item => item.id.Equals(itemId));
        }

        /// <inheritdoc />
        public DMSItem getByFilename(string filename)
        {
            //todo: optimize (linq/lambda)
            return this.items.Single(item => item.filename.Equals(filename));
        }
    }
}
