﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace de.tarent.dms.dmsapi.mock
{
    /// <summary>
    /// a mock implementation of SPServices
    /// </summary>
    [XmlRootAttribute("MockData")]
    public class MockServices : DMSServices
    {
        /// <summary>
        /// Load SPServices for the mock implementation based on a xml configuration file
        /// </summary>
        public static MockServices load(String configData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MockServices));
            FileStream fs = null;
            try
            {
                fs = new FileStream(configData, FileMode.Open);
                MockServices spServices = (MockServices)serializer.Deserialize(fs);
                spServices.init(Directory.GetParent(configData).FullName + "\\");
                return spServices;
            }
            finally
            {
                if (fs != null) fs.Close();
            }
        }

        /// <summary>
        /// Array of mock repositories to simulate the server
        /// </summary>
        [XmlArrayAttribute("Repositories")]
        public MockRepository[] mockRepos;

        /// <summary>
        /// for xml deserialize
        /// </summary>
        private MockServices() { }

        /// <inheritdoc />
        public DMSRepository getRepository(string url)
        {
            foreach (DMSRepository repo in mockRepos) if (repo.url.Equals(url)) return repo;
            throw new DMSException(DMSException.ENDPOINT_NOT_FOUND, "Mock-Services didn't find a valid repository for: " + url);
        }

        /// <summary>
        /// Initialization of all loaded MockRepos with the given workingDirPath
        /// </summary>
        private void init(String workingDirPath)
        {
            foreach (DMSRepository repo in mockRepos)
                ((MockRepository)repo).initMockRepo(workingDirPath);
        }

        /// <summary>
        /// Cleanup all loaded MockRepos -> cleaning the ghost-dir
        /// </summary>
        public void cleanup()
        {
            foreach (DMSRepository repo in mockRepos)
                ((MockRepository)repo).cleanup();
        }
    }
}
