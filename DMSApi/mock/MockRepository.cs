﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;

namespace de.tarent.dms.dmsapi.mock
{
    /// <inheritdoc />
    [XmlType("Repository")]
    public class MockRepository : DMSRepository
    {
        /// <inheritdoc />
        [XmlAttribute("title")]
        public String title { get; set; }

        /// <inheritdoc />
        [XmlAttribute("url")]
        public String url { get; set; }

        /// <summary>
        /// a collection of splibraries (mock)
        /// </summary>
        [XmlArrayAttribute("Libs", IsNullable = false)]
        public List<MockLibrary> mock_libs;

        /// <inheritdoc />
        [XmlAttribute("localPath")]
        public String localTemplatePath;

        /// <inheritdoc />
        [XmlIgnore]
        public String localWorkingPath;

        private MockRepository()
        {
        }

        /// <summary>
        /// copies the content of a given local folder to a working directory to simulate a server
        /// </summary>
        internal void initMockRepo(String workingDirPath)
        {
            if (this.localTemplatePath == null)
                throw new DMSException(DMSException.CONFIGURATION_ERROR, "the local folder was not defined in xml, " + this);
            
            this.localTemplatePath = workingDirPath + this.localTemplatePath;

            if (!Directory.Exists(localTemplatePath))
                throw new DMSException(DMSException.CONFIGURATION_ERROR, "the local folder was not found, localTemplatePath=" + localTemplatePath);

            this.localWorkingPath = localTemplatePath + "_ghost";

            copyFolder(localTemplatePath, localWorkingPath);

            foreach (MockLibrary lib in this.mock_libs)
            {
                lib.spRepository = this;
                foreach (MockItem item in lib.mock_items) item.spLibrary = lib;
            }
        }

        internal void cleanup()
        {
            if (Directory.Exists(this.localWorkingPath))
                Directory.Delete(this.localWorkingPath, true);
        }

        private void copyFiles(String sourcePath, String targetPath)
        {
            string[] files = System.IO.Directory.GetFiles(sourcePath);

            String fileName;
            String destFile;

            foreach (string s in files)
            {
                // Use static Path methods to extract only the file name from the path.
                fileName = System.IO.Path.GetFileName(s);
                destFile = System.IO.Path.Combine(targetPath, fileName);
                System.IO.File.Copy(s, destFile, true);
            }
        }

        private void copyFolder(String source, String destitation)
        {
            if (source.Contains(".svn")) return;
            
            if (!Directory.Exists(destitation))
                Directory.CreateDirectory(destitation);

            string[] files = Directory.GetFiles(source);
            foreach (string file in files)
            {
                string dest = Path.Combine(destitation, Path.GetFileName(file));
                File.Copy(file, dest, true);
            }

            string[] folders = Directory.GetDirectories(source);
            foreach (string folder in folders)
            {
                string dest = Path.Combine(destitation, Path.GetFileName(folder));
                copyFolder(folder, dest);
            }
        }

        /// <inheritdoc />
        internal MockRepository(String title, String url)
        {
            this.title = title;
            this.url = url;
        }

        /// <summary>
        /// returns a mock list of splibraries
        /// </summary>
        List<DMSLibrary> DMSRepository.libraries
        {
            get 
            {
                List<DMSLibrary> newList = new List<DMSLibrary>();
                foreach (MockLibrary lib in mock_libs)
                {
                    newList.Add(lib);
                }
                return newList;
            }
        }

        /// <inheritdoc />
        DMSLibrary DMSRepository.getLibraryByTitle(string title)
        {
            return mock_libs.Single(lib => lib.title.Equals(title));
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return GetType().Name + "[title=" + this.title + ", url=" + this.url + "]";
        }
    }
}
