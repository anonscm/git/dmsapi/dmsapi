﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using de.tarent.dms.dmsapi.utils;
using de.tarent.dms.dmsapi;

namespace de.tarent.dms.dmsapi.mock
{
    /// <summary>
    /// a mock implementation of DMSLibrary
    /// </summary>
    [XmlType("Lib")]
    public class MockLibrary : DMSLibrary
    {
        /// <inheritdoc />
        public DMSRepository repository { get { return spRepository; } }
        internal MockRepository spRepository { get; set; }

        /// <inheritdoc />
        [XmlAttribute("title")]
        public String title { get; set; }

        /// <inheritdoc />
        [XmlAttribute("id")]
        public String id { get; set; }

        /// <inheritdoc />
        [XmlArrayAttribute("SPItems", IsNullable = false)]
        public List<MockItem> mock_items;

        /// <inheritdoc />
        [XmlIgnore]
        public List<DMSItem> items 
        { 
            get
            {
                List<DMSItem> tempList = new List<DMSItem>();
                foreach (MockItem item in mock_items)
                    tempList.Add(item.copy());
                return tempList;
            }
        }

        /// <inheritdoc />
        public String CHECKINTYPE_MINOR { get { return "0"; } }

        /// <inheritdoc />
        public String CHECKINTYPE_MAJOR { get { return "1"; } }

        /// <inheritdoc />
        public String CHECKINTYPE_OVERWRITE { get { return "2"; } }

        /// <summary>
        /// constructor for xml de-/serialisation
        /// </summary>
        private MockLibrary() { }

        /// <inheritdoc />
        public DMSItem create(AdvDictionary<String, String> itemMetadata)
        {
            MockItem item = new MockItem(this, new AdvDictionary<String, String>(itemMetadata));
            item.id = Guid.NewGuid().ToString();
            this.mock_items.Add(item);
            return item.copy();
        }

        /// <inheritdoc />
        public DMSItem create(String itemFilename, AdvDictionary<String, String> itemMetadata, String pathToFile)
        {
            File.Copy(pathToFile, getWorkingDirFilePath(itemFilename), true);
            MockItem item;
            try
            {
                item = (MockItem)this.items.Single(newItem => itemFilename.Equals(newItem.filename));
                item.metadata = itemMetadata;
                item.filename = itemFilename;
            }
            catch (Exception)
            {
                item = new MockItem(this, new AdvDictionary<String, String>(itemMetadata));
                item.id = Guid.NewGuid().ToString();
                item.filename = itemFilename;
                this.mock_items.Add(item);
            }
            return item.copy();
        }

        /// <inheritdoc />
        public void delete(DMSItem item)
        {
            MockItem deleteItem;

            // try delete in lib
            try
            {
                deleteItem = this.mock_items.Single(spItem => item.filename.Equals(spItem.filename));
                File.Delete(getWorkingDirFilePath(deleteItem));
                this.mock_items.Remove(deleteItem);
            }
            catch (Exception)
            {
                // try delete in list
                try
                {
                    deleteItem = this.mock_items.Single(spItem => item.title.Equals(spItem.title));
                    this.mock_items.Remove(deleteItem);
                }
                catch (Exception e)
                {
                    throw new DMSException(DMSException.FUNCTIONAL_ERROR, "delete() failed, item not found, item=" + item, e);
                }
            }
        }

        /// <inheritdoc />
        public Stream download(DMSItem iSPItem)
        {
            return new FileStream(getWorkingDirFilePath(iSPItem), FileMode.Open, FileAccess.Read);
        }

        /// <inheritdoc />
        public void download(DMSItem iSPItem, String localpath)
        {
            File.Copy(getWorkingDirFilePath(iSPItem), localpath, true);
        }

        /// <inheritdoc />
        public bool checkOut(DMSItem iSPItem)
        {
            if (iSPItem.filename == null) throw new DMSException(DMSException.FUNCTIONAL_ERROR, "checkOut list item isn't possible" + iSPItem);
            
            DMSItem checkOutItem = null;
            try
            {
                checkOutItem = mock_items.Single(item => iSPItem.filename.Equals(item.filename));
            }
            catch {
                return false;
            }
            checkOutItem.setMetadata("CheckoutUser", "mock_user");
            return true;
        }

        /// <inheritdoc />
        //public void upload(ISPItem iSPItem, String localpath, Boolean pathFromItem, Dictionary<String, String> fieldsAndValues)
        public void upload(DMSItem item, string fromLocalPath, bool alsoUpdateMetadata)
        {
            foreach (DMSItem titem in mock_items)
                if (titem.filename.Equals(item.filename))
                {
                    mock_items.Remove((MockItem)titem);
                    break;
                }
            mock_items.Add((MockItem)item);

            File.Copy(fromLocalPath, getWorkingDirFilePath(item), true);
        }

        /// <inheritdoc />
        public void upload(DMSItem item, Stream content, bool updateMetadata)
        {
            foreach (DMSItem titem in mock_items)
                if (titem.filename.Equals(item.filename))
                {
                    mock_items.Remove((MockItem)titem);
                    break;
                }
            mock_items.Add((MockItem)item);

            String path = getWorkingDirFilePath(item);

            if (File.Exists(path))
                File.Delete(path);

            FileStream file = File.Create(path);

            content.Position = 0;

            const int size = 4096;
            byte[] bytes = new byte[4096];
            int numBytes;
            while ((numBytes = content.Read(bytes, 0, size)) > 0)
                file.Write(bytes, 0, numBytes);
            file.Flush();
            file.Close();
        }

        /// <inheritdoc />
        public bool checkIn(DMSItem iSPItem, String comment, String checkInType)
        {
            if (iSPItem.filename == null) throw new DMSException(DMSException.FUNCTIONAL_ERROR, "checkIn list item isn't possible" + iSPItem);
            
            DMSItem checkInItem = null;
            try
            {
                checkInItem = mock_items.Single(item => iSPItem.filename.Equals(item.filename));
            }
            catch
            {
                return false;
            }

            checkInItem.setMetadata("CheckoutUser", "");
            double version = Convert.ToDouble(checkInItem.getMetadata("Version"));
            if (checkInType == CHECKINTYPE_MAJOR)
                version += 1;
            if (checkInType == CHECKINTYPE_MINOR)
                version += 0.1;

            return true;
        }

        /// <inheritdoc />
        public void undoCheckOut(DMSItem iSPItem)
        {
            if (iSPItem.filename == null) throw new DMSException(DMSException.FUNCTIONAL_ERROR, "undoCheckOut list item isn't possible" + iSPItem);
            if (!checkIn(iSPItem, null, CHECKINTYPE_OVERWRITE))
                throw new DMSException(DMSException.UNEXPECTED_ERROR, "undoCheckOut(): could not checkIn item");
        }

        /// <inheritdoc />
        public void update(DMSItem iSPItem)
        {
            foreach (DMSItem item in mock_items)
                if (item.id.Equals(iSPItem.id))
                {
                    item.metadata = iSPItem.metadata;
                    break;
                }
        }

        /// <inheritdoc />
        public DMSItem getByID(String itemId)
        {
            return this.items.SingleOrDefault(item => itemId.Equals(item.id));
        }

        /// <inheritdoc />
        public DMSItem getByFilename(String itemFilename)
        {
            return this.items.SingleOrDefault(item => itemFilename.Equals(item.filename));
        }

        private String getWorkingDirFilePath(DMSItem item)
        {
            return getWorkingDirFilePath(item.filename);
        }

        private String getWorkingDirFilePath(String itemFilename)
        {
            return this.spRepository.localWorkingPath + @"\" + this.title + @"\" + itemFilename;
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj is MockLibrary)
            {
                MockLibrary otherLib = (MockLibrary)obj;
                return String.Equals(this.id, otherLib.id) && String.Equals(this.title, otherLib.title);
            }
            return false;
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            int hashCode = 7;
            hashCode = hashCode * 31 + (id != null ? id.GetHashCode() : 0);
            hashCode = hashCode * 31 + (title != null ? title.GetHashCode() : 0);
            return hashCode;
        }

        /// <inheritdoc />
        public override String ToString()
        {
            return GetType().Name + "["
                + "title=" + title
                + ", id=" + id
                + "]";
        }
    }
}
