﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace de.tarent.dms.dmsapi.utils
{
    class FileUtils
    {
        /// <summary>
        /// reads a file for the given path into an array of bytes
        /// </summary>
        public static byte[] fileToByte(string path)
        {
            FileStream fileStream = new FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read);

            int length = (int)fileStream.Length;  // get file length
            byte[] buffer = new byte[length];     // create buffer
            int count;                            // actual number of bytes read
            int sum = 0;                          // total number of bytes read

            try
            {
                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }

            return buffer;
        }

        /// <summary>
        /// Copies one Stream to another
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
            output.Seek(0,SeekOrigin.Begin);
        }
        
        /// <summary>
        /// saves a stream to a file
        /// </summary>
        /// <param name="inputStream"></param>
        /// <param name="outputFile"></param>
        /// <param name="fileMode"></param>
        public static void StreamToFile(Stream inputStream, string outputFile)
        {
            using (Stream file = File.OpenWrite(outputFile))
            {
                CopyStream(inputStream, file);
            }
        }

        /// <summary>
        /// reads a stream to a byte array
        /// </summary>
        public static byte[] streamToByte(Stream stream)
        {
            long originalPosition = stream.Position;
            stream.Position = 0;

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                stream.Position = originalPosition;
            }
        }
    }
}
