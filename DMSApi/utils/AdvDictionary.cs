﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'SPApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace de.tarent.dms.dmsapi.utils
{
    [XmlRoot("dictionary")]
    public class AdvDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        /// <inheritdoc />
        public AdvDictionary() {}

        /// <inheritdoc />
        public AdvDictionary(Dictionary<TKey, TValue> dict) : base(dict) { }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            if (null == obj || !(obj is AdvDictionary<TKey, TValue>)) return false;

            AdvDictionary<TKey, TValue> otherDict = (AdvDictionary<TKey, TValue>)obj;

            if (this.Count != otherDict.Count) return false;

            foreach (KeyValuePair<TKey, TValue> kvp in this)
            {
                TValue otherKeyValue;
                if (!otherDict.TryGetValue(kvp.Key, out otherKeyValue)) return false;
                if (!kvp.Value.Equals(otherKeyValue)) return false;
            }

            return true;
        } 

        /// <inheritdoc />
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <inheritdoc />
        public void ReadXml(System.Xml.XmlReader reader)
        {
            if (reader.IsEmptyElement) return;
                        
            reader.Read();

            // check for string optimation
            if (typeof(TKey).Equals(typeof(String)) && typeof(TValue).Equals(typeof(String)))
            {
                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");
                    reader.ReadStartElement("key");
                    TKey key = (TKey)(Object)reader.ReadContentAsString();
                    reader.ReadEndElement();
                    reader.ReadStartElement("value");
                    TValue value = (TValue)(Object)reader.ReadContentAsString();
                    reader.ReadEndElement();
                    this.Add(key, value);
                    reader.ReadEndElement();
                    reader.MoveToContent();
                }
            }
            else
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    reader.ReadStartElement("item");
                    reader.ReadStartElement("key");
                    TKey key = (TKey)keySerializer.Deserialize(reader);
                    reader.ReadEndElement();
                    reader.ReadStartElement("value");
                    TValue value = (TValue)valueSerializer.Deserialize(reader);
                    reader.ReadEndElement();
                    this.Add(key, value);
                    reader.ReadEndElement();
                    reader.MoveToContent();
                }
            }
            reader.ReadEndElement();
        }


        /// <inheritdoc />
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            // check for string optimation
            if (typeof(TKey).Equals(typeof(String)) && typeof(TValue).Equals(typeof(String)))
            {
                foreach (KeyValuePair<TKey, TValue> kvp in this)
                {
                    writer.WriteStartElement("item");
                    writer.WriteStartElement("key");
                    writer.WriteString((String)(Object)kvp.Key);
                    writer.WriteEndElement();
                    writer.WriteStartElement("value");
                    writer.WriteString((String)(Object)kvp.Value);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
            else
            {
                XmlSerializer keySerializer = new XmlSerializer(typeof(TKey));
                XmlSerializer valueSerializer = new XmlSerializer(typeof(TValue));

                foreach (KeyValuePair<TKey, TValue> kvp in this)
                {
                    writer.WriteStartElement("item");
                    writer.WriteStartElement("key");
                    keySerializer.Serialize(writer, kvp.Key);
                    writer.WriteEndElement();
                    writer.WriteStartElement("value");
                    valueSerializer.Serialize(writer, kvp.Value);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
        }
    }
}
