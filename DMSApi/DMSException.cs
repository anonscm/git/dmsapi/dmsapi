﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace de.tarent.dms.dmsapi
{
    /// <summary>
    /// SPException for Exception in SPApi
    /// </summary>
    public class DMSException : Exception
    {
        /// <summary>
        /// Errorcode for FUNCTIONAL ERRORs
        /// </summary>
        public static int FUNCTIONAL_ERROR = 1;

        /// <summary>
        /// Errorcode for UNEXPECTED ERRORs
        /// </summary>
        public static int UNEXPECTED_ERROR = 2;

        /// <summary>
        /// Errorcode for CONFIGURATION ERRORs
        /// </summary>
        public static int CONFIGURATION_ERROR = 3;

        /// <summary>
        /// Errorcode for ENDPOINT ERRORs
        /// </summary>
        public static int ENDPOINT_NOT_FOUND = 4;

        /// <summary>
        /// Errorcode for NOT AUTHORIZED ERRORs
        /// </summary>
        public static int NOT_AUTHORIZED = 5;

        /// <summary>
        /// Errorcode
        /// </summary>
        public int errorcode { get { return _errorcode; } }
        private int _errorcode;

        /// <summary>
        /// Constructor
        /// </summary>
        public DMSException(int error_code, String message)
            : base(message)
        {
            this._errorcode = error_code;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public DMSException(int error_code, String message, Exception innerException)
            : base(message, innerException)
        {
            this._errorcode = error_code;
        }
    }
}
