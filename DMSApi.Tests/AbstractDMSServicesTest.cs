﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using de.tarent.dms.dmsapi;

namespace de.tarent.dms.dmsapi.test
{
    [TestClass()]
    public abstract class AbstractDMSServicesTest
    {
        protected DMSServices spServices;
        protected String repositoryUrl;

        [TestMethod()]
        public void checkAndSetupMembers()
        {
            Assert.IsNotNull(spServices);
            Assert.IsNotNull(repositoryUrl);
        }

        [TestMethod()]
        public void validRepository()
        {
            DMSRepository repository = spServices.getRepository(repositoryUrl);
            Assert.IsNotNull(repository);
        }

        [TestMethod()]
        public void invalidRepository()
        {
            String randomString = Guid.NewGuid().ToString();

            try
            {
                DMSRepository repository = spServices.getRepository("http://" + randomString);
                Assert.Fail("you shall never reach this point");
            }
            catch (DMSException exception)
            {
                Assert.IsTrue(exception.errorcode == DMSException.ENDPOINT_NOT_FOUND || exception.errorcode == DMSException.CONFIGURATION_ERROR);
            }
        }
    }
}
