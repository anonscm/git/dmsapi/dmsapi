﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace de.tarent.dms.dmsapi.test.spClientOM
{
    internal class SPOnlineConfig
    {
        private static Config config = new Config("../../../DMSApi.Tests/sharepointClientOM/spOnline.cfg");

        public static String repositoryUrl = config.get("repositoryUrl");

        public static String userName = config.get("userName");

        public static String password = config.get("password");

        // make sure these libs exists in the repo
        public static String libraryTitle = config.get("libraryTitle");
        public static String listTitle = config.get("listTitle");

        // make sure this item exists in the libs
        public static String libItemTitle = config.get("libItemTitle");
        public static String listItemTitle = config.get("listItemTitle");

        public static String itemFilename = config.get("itemFilename");

        public static String createItemFilename = config.get("createItemFilename");

        public static String createItemFilepath = config.get("createItemFilepath");
    }
}
