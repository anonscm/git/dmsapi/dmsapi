﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using de.tarent.dms.dmsapi.utils;
using de.tarent.dms.dmsapi.spWebServices;

namespace de.tarent.dms.dmsapi.test.spWebServices
{
    [TestClass]
    public class SPWSItemTest : AbstractDMSItemTest
    {
        public SPWSItemTest()
        {
            this.testItem = new SPWSItem(null, new AdvDictionary<String, String>());
        }
    }
}
