﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using de.tarent.dms.dmsapi.mock;

namespace de.tarent.dms.dmsapi.test.mock
{
    [TestClass()]
    public class MockLibraryTest : AbstractDMSLibraryTest
    {
        private MockServices spService;

        public MockLibraryTest()
        {
            this.spService = MockServices.load("../../../DMSApi.Tests/mock/data/xmldata.xml");
            DMSRepository spRepository = spService.getRepository("http://tarent.de/mock/testrepo");
            this.spLibrary = spRepository.getLibraryByTitle("testlib");
            this.localpath = "./";
            this.itemFilename = "test.docx";
            this.createItemFilename = "create_test_item.docx";
            this.createItemFilepath = "../../../DMSApi.Tests/resources/create_test_item_file.docx";
        }

        [TestCleanup()]
        public override void cleanUp()
        {
            base.cleanUp();
            spService.cleanup();
        }
    }
}
