﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using de.tarent.dms.dmsapi.utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace de.tarent.dms.dmsapi.test
{
    /// <summary>
    /// Define tests for the ISPLibrary Interface operating on a SP-Libaray
    /// </summary>
    [TestClass()]
    public abstract class AbstractDMSLibraryTest
    {
        private DMSItem testItem;

        // default settings
        protected DMSLibrary spLibrary;
        protected String localpath;
        protected String itemFilename;

        // settings for create item test
        protected String createItemFilename;
        protected String createItemFilepath;
        private DMSItem createdItem;

        // settings for download item test
        private String downloadFilename;

        [TestInitialize()]
        public void checkMembers()
        {
            Assert.IsNotNull(this.spLibrary, "this.spLibrary must not be null!");
            Assert.IsNotNull(this.localpath, "this.localpath must not be null!");
            Assert.IsNotNull(this.itemFilename, "this.itemFilename must not be null!");

            Assert.IsNotNull(this.createItemFilename, "this.createItemFilename must not be null!");
            Assert.IsNotNull(this.createItemFilepath, "this.createItemFilepath must not be null!");

            Assert.IsTrue(Directory.Exists(this.localpath), "this.localpath must exist! localpath=" + this.localpath);

            // create directory for testing
            this.localpath += "testing";
            Directory.CreateDirectory(this.localpath);
            Assert.IsTrue(Directory.Exists(this.localpath), "this.localpath must exist! localpath=" + this.localpath);

            try
            {
                this.testItem = this.spLibrary.items.Single(item => this.itemFilename.Equals(item.filename));
            }
            catch (InvalidOperationException)
            {
                Assert.Fail("item must be found for filename=" + this.itemFilename);
            }
        }

        [TestCleanup()]
        public virtual void cleanUp()
        {
            Directory.Delete(this.localpath, true);
            Assert.IsFalse(Directory.Exists(this.localpath), "this.localpath must be removed! localpath=" + this.localpath);
        }

        [TestMethod()]
        public void testLibraryItems()
        {
            List<DMSItem> items = this.spLibrary.items;
            Assert.IsNotNull(items, "list of items must not be null!");
            Assert.IsTrue(items.Count > 0, "list of items must have one or more elements!");
        }

        [TestMethod()]
        public void testCreateLibItem()
        {
            //todo: make sure it doesnt already exist

            String createTitle = "create.item.title";
            AdvDictionary<String, String> metadata = new AdvDictionary<String, String>();
            metadata.Add("Title", createTitle);

            this.createdItem = this.spLibrary.create(this.createItemFilename, metadata, this.createItemFilepath);

            Assert.IsNotNull(this.createdItem, "the item must be created!");
            Assert.IsTrue(this.spLibrary.Equals(this.createdItem.library), "the item must from the same library it was created by!");
            Assert.IsTrue(this.createItemFilename.Equals(this.createdItem.filename), "the item's filename must be the same as given by creation!");
            Assert.IsTrue(createTitle.Equals(this.createdItem.title), "the item's title must be the same as given by creation!");
            
            try
            {
                this.spLibrary.items.Single(item => this.createItemFilename.Equals(item.filename));
            }
            catch (InvalidOperationException)
            {
                Assert.Fail("created item must be found for filename=" + this.createItemFilename);
            }

            //todo: make sure it isnt checked or something

            //todo: delete it
        }

        [TestMethod()]
        public void testUpdateItemTitle()
        {
            testCreateLibItem();

            String newItemTitle = "new item title";
            String createdItemTitle = this.createdItem.title;
            Assert.IsFalse(newItemTitle.Equals(createdItemTitle), "newItemTitle must not match createdItemTitle before upgrading");
            this.createdItem.setMetadata("Title", newItemTitle);
            this.spLibrary.update(this.createdItem);
            DMSItem updatedItem = this.spLibrary.items.Single(item => this.createdItem.filename.Equals(item.filename));
            String updatedItemTitle = updatedItem.title;
            Assert.IsTrue(newItemTitle.Equals(updatedItemTitle), "newItemTitle must match updatedItemTitle after upgrading");
        }

        [TestMethod()]
        public void testDeleteLibItem()
        {
            testCreateLibItem();

            this.spLibrary.delete(this.createdItem);

            try
            {
                this.spLibrary.items.Single(item => this.createdItem.filename.Equals(item.filename));
                Assert.Fail("item must not be found for filename=" + this.createdItem.filename);
            }
            catch (InvalidOperationException) {
                // should happen! 
            }
        }

        [TestMethod()]
        public void testdownloadItems()
        {
            this.downloadFilename = this.localpath + "/" + this.testItem.filename;

            if (File.Exists(downloadFilename))
            {
                File.Delete(downloadFilename);
                Assert.IsFalse(File.Exists(downloadFilename), "file already exists and could not be deleted: " + downloadFilename);
            }

            this.spLibrary.download(this.testItem, downloadFilename);

            Assert.IsTrue(File.Exists(downloadFilename), "file wasn't downloaded to: " + downloadFilename);
        }

        [TestMethod()]
        public void testUploadItemFromFile()
        {
            String path = createItemFilepath;
            Assert.IsTrue(File.Exists(path));

            this.spLibrary.upload(this.testItem, path, false);


            //todo: download and compare 
            //this.downloadFilename2 = this.downloadFilename + "_test";

            //this.spLibrary.download(this.testItem, this.downloadFilename2);

            //todo: this was fileassert in nunit. reimplement it
            //Assert.AreEqual(new FileStream(downloadFilename, FileMode.Open), new FileStream(downloadFilename2, FileMode.Open), "uploaded and downloaded files must be equal!");
        }

        [TestMethod()]
        public void testUploadItemFromStream()
        {
            String path = createItemFilepath;
            Assert.IsTrue(File.Exists(path));
            using (Stream stream = new FileStream(path, FileMode.Open))
            {
                this.spLibrary.upload(this.testItem, stream, false);
            }

            //todo: download and compare 
            //this.downloadFilename2 = this.downloadFilename + "_test";

            //this.spLibrary.download(this.testItem, this.downloadFilename2);

            //todo: this was fileassert in nunit. reimplement it
            //Assert.AreEqual(new FileStream(downloadFilename, FileMode.Open), new FileStream(downloadFilename2, FileMode.Open), "uploaded and downloaded files must be equal!");
        }

        [TestMethod()]
        public void testCheckOutCheckInOperations()
        {
            Boolean checkIOSuccessful;
            DMSItem checkIOItem;
            String checkedOutBy;

            //check out
            checkIOSuccessful = spLibrary.checkOut(testItem);
            Assert.IsTrue(checkIOSuccessful);
            checkIOItem = spLibrary.items.Single(myItem => myItem.filename.Equals(testItem.filename));
            checkedOutBy = checkIOItem.checkedOutBy;
            Assert.IsTrue(checkedOutBy != null || checkedOutBy.Length > 1);

            //try check out again (should fail)
            checkIOSuccessful = spLibrary.checkOut(testItem);
            Assert.IsFalse(checkIOSuccessful);
            
            //undo
            spLibrary.undoCheckOut(testItem);
            checkIOItem = spLibrary.items.Single(myItem => myItem.filename.Equals(testItem.filename));
            checkedOutBy = checkIOItem.checkedOutBy;
            Assert.IsTrue(checkedOutBy == null || checkedOutBy.Length < 1);

            //check out again
            Assert.IsTrue(spLibrary.checkOut(testItem));

            //check in
            checkIOSuccessful = spLibrary.checkIn(this.testItem, " checkedin by testcase", this.spLibrary.CHECKINTYPE_MINOR);
            Assert.IsTrue(checkIOSuccessful);
        }

        [TestMethod()]
        public void testGetItemByID()
        {
            DMSItem idItem = this.spLibrary.getByID(this.testItem.id);

            Assert.IsNotNull(idItem, "idItem must not be null");
            Assert.AreEqual(this.testItem.id, idItem.id, "testItem's and idItem's ID must be the equals");
        }

        [TestMethod()]
        public void testGetByFilename()
        {
            DMSItem filenameItem = this.spLibrary.getByFilename(this.testItem.filename);

            Assert.IsNotNull(filenameItem, "filenameItem must not be null");
            Assert.AreEqual(this.testItem.filename, filenameItem.filename, "testItem's and filenameItem's filename must be the equals");
        }
    }
}
