﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using de.tarent.dms.dmsapi.utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace de.tarent.dms.dmsapi.test
{
    /// <summary>
    /// Define tests for the ISPLibrary Interface operating on a SP-List
    /// </summary>
    [TestClass()]
    public abstract class AbstractDMSListTest
    {
        protected DMSLibrary testList;
        protected String testItemTitle;

        private DMSItem testItem;
        private DMSItem createdItem;

        [TestInitialize()]
        public void checkMembers()
        {
            // check config
            Assert.IsNotNull(this.testList, "this.testList must not be null!");
            Assert.IsNotNull(this.testItemTitle, "this.testItemTitle must not be null!");

            // try get test item
            try
            {
                this.testItem = this.testList.items.Single(item => this.testItemTitle.Equals(item.title));
            }
            catch (InvalidOperationException)
            {
                Assert.Fail("item must be found for title=" + this.testItemTitle);
            }
        }

        [TestMethod()]
        public void testLibraryItems()
        {
            List<DMSItem> items = this.testList.items;
            Assert.IsNotNull(items, "list of items must not be null!");
            Assert.IsTrue(items.Count > 0, "list of items must have one or more elements!");
        }

        [TestMethod()]
        public void testCreateUpdateDelete()
        {
            //create
            String createTitle = "testitem_" + DateTime.Now.ToString();
            AdvDictionary<String, String> metadata = new AdvDictionary<String, String>();
            metadata.Add("Title", createTitle);

            createdItem = testList.create(metadata);

            Assert.IsNotNull(createdItem, "the item must be created!");
            Assert.IsTrue(testList.Equals(createdItem.library), "the item must from the same library it was created by!");
            Assert.IsTrue(createTitle.Equals(createdItem.title), "the item's title must be the same as given by creation!");

            Assert.IsNotNull(testList.items.Single(item => createTitle.Equals(item.title)));
         
            //update
            String newItemTitle = "new item title";
            Assert.AreNotEqual(newItemTitle, this.createdItem.title, "newItemTitle must not match createdItemTitle before upgrading");

            createdItem.setMetadata("Title", newItemTitle);
            testList.update(createdItem);

            DMSItem updatedItem = testList.items.Single(item => createdItem.title.Equals(item.title));
            Assert.AreEqual(newItemTitle, updatedItem.title, "newItemTitle must match updatedItemTitle after upgrading");
       
            //delete
            testList.delete(createdItem);

            try
            {
                this.testList.items.Single(item => this.createdItem.title.Equals(item.title));
                Assert.Fail("item must not be found for title=" + this.createdItem.title);
            }
            catch (InvalidOperationException)
            {
                // should happen! 
            }
        }

        [TestMethod()]
        public void testCheckOut()
        {
            try
            {
                this.testList.checkOut(this.testItem);
                Assert.Fail("checkout a sp-list item must fail with exception");
            }
            catch (DMSException spe)
            {
                Assert.AreEqual(DMSException.FUNCTIONAL_ERROR, spe.errorcode, "error code must be SPException.FUNCTIONAL_ERROR");
            }
        }

        [TestMethod()]
        public void testCheckIn()
        {
            try
            {
                this.testList.checkIn(this.testItem, "t06_checkIn() test", this.testList.CHECKINTYPE_MINOR);
                Assert.Fail("checkin a sp-list item must fail with exception");
            }
            catch (DMSException spe)
            {
                Assert.AreEqual(DMSException.FUNCTIONAL_ERROR, spe.errorcode, "error code must be SPException.FUNCTIONAL_ERROR");
            }
        }

        [TestMethod()]
        public void testUndoCheckIn()
        {
            try
            {
                this.testList.undoCheckOut(this.testItem);
                Assert.Fail("undo checkout a sp-list item must fail with exception");
            }
            catch (DMSException spe)
            {
                Assert.AreEqual(DMSException.FUNCTIONAL_ERROR, spe.errorcode, "error code must be SPException.FUNCTIONAL_ERROR");
            }
        }
    }
}
