﻿// SPApi
// The SPApi libaray is an abstraction layer for the different versions 
// and typs of SharePoint Web-Services.
// Copyright (C) 05.05.2010 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'SPApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;

using System.Configuration;
using System.Reflection;

namespace de.tarent.dms.dmsapi.test
{
    /// <summary>
    /// read config file
    /// </summary>
    public class Config
    {
        private Dictionary<String, String> keyValuePairs = new Dictionary<String, String>();

        public Config(String filename)
        {
            if (File.Exists(filename))
            {
                using (StreamReader configReader = new StreamReader(filename))
                {
                    String line;
                    while ((line = configReader.ReadLine()) != null)
                    {
                        String[] keyValue = line.Split(new String[] { "=" }, StringSplitOptions.None);
                        keyValuePairs.Add(keyValue[0], keyValue[1]);
                    }
                }                
            }
            else
            {
                throw new FileNotFoundException("Config() failed, config file=" + filename + " not found");
            }
        }

        /// <summary>
        /// return the config value for the key
        /// </summary>
        public String get(String key)
        {
            String value;
            return keyValuePairs.TryGetValue(key, out value) ? value : null;
        }
    }
}
