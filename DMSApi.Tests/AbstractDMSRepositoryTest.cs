﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace de.tarent.dms.dmsapi.test
{
    /// <summary>
    /// Define default tests for the ISPRepository Interface
    /// </summary>
    [TestClass()]
    public abstract class AbstractDMSRepositoryTest
    {
        protected DMSServices spServices;
        protected String repositoryUrl;
        protected DMSRepository spRepository;
        protected String libraryTitle;

        [TestInitialize()]
        public void checkAndSetupMembers()
        {
            Assert.IsNotNull(this.spServices, "this.spServices must not be null!");

            Assert.IsNotNull(this.repositoryUrl, "this.repositoryUrl must not be null!");

            this.spRepository = this.spServices.getRepository(this.repositoryUrl);

            Assert.IsNotNull(this.spRepository, "this.spRepository must not be null!");

            Assert.IsNotNull(this.libraryTitle, "this.libraryTitle must not be null!");
        }

        [TestMethod()]
        public void repoTitle()
        {
            Assert.IsNotNull(spRepository.title, "repository title must not be null!");
        }

        [TestMethod()]
        public void repoUrl()
        {
            Assert.IsNotNull(spRepository.url, "repository url must not be null!");
            Assert.IsTrue(repositoryUrl.Equals(spRepository.url), "repository url must be the same like the url for loading the reposetory!");
        }

        [TestMethod()]
        public void repoLibs()
        {
            List<DMSLibrary> libraries = spRepository.libraries;
            Assert.IsTrue(libraries.Count > 0, "repository's libraries must be more then zero!");
        }

        [TestMethod()]
        public void libByTitle()
        {
            DMSLibrary library = spRepository.getLibraryByTitle(libraryTitle);
            Assert.IsNotNull(library, "library must not be null!");
        }
    }
}
