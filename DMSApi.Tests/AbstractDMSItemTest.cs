﻿// DMSApi
// The DMSApi library is an abstraction layer for the different versions 
// and types of DMS-Servers like Microsofts Sharepoint-Server.
// Copyright (C) 2011 tarent GmbH
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,version 2
// as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
// tarent GmbH., hereby disclaims all copyright
// interest in the program 'DMSApi'
// Signature of Elmar Geese, 05.05.2010
// Elmar Geese, CEO tarent GmbH.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace de.tarent.dms.dmsapi.test
{
    [TestClass()]
    public abstract class AbstractDMSItemTest
    {
        protected DMSItem testItem;

        private String randomString = Guid.NewGuid().ToString();
        private String anotherRandomString = Guid.NewGuid().ToString();
        
        [TestInitialize()]
        public void checkMembers()
        {
            Assert.IsNotNull(this.testItem, "this.testItem must not be null!");
        }

        [TestCleanup()]
        public void cleanUp()
        {
            // do nothing
        }

        [TestMethod()]
        public void testMetaFieldNotSet()
        {
            Assert.IsNull(testItem.getMetadata(randomString));
        }

        [TestMethod()]
        public void testSetAndClearMetaField()
        {
            //set field
            testItem.setMetadata(randomString, randomString);
            String meta = testItem.getMetadata(randomString);
            Assert.IsNotNull(meta);
            Assert.IsTrue(randomString.Equals(meta));
            
            //clear field
            testItem.setMetadata(randomString, null);
            Assert.IsNull(testItem.getMetadata(randomString));
        }

        [TestMethod()]
        public void testOverWriteMetaField()
        {
            testItem.setMetadata(randomString, randomString);
            testItem.setMetadata(randomString, anotherRandomString);
            Assert.AreEqual(anotherRandomString, testItem.getMetadata(randomString));
            
            //clear field
            testItem.setMetadata(randomString, null);
        }


        [TestMethod]
        public void testCopyItem()
        {
            testItem.setMetadata(randomString, randomString);
            DMSItem copy = testItem.copy();

            // equality
            Assert.IsNotNull(copy);
            Assert.IsTrue(copy.Equals(testItem));
            Assert.IsTrue(testItem.getMetadata(randomString).Equals(copy.getMetadata(randomString)));

            // inequality
            copy.setMetadata(randomString, anotherRandomString);
            Assert.IsFalse(copy.Equals(testItem));
            
            //clean up
            testItem.setMetadata(randomString, null);           
        }
    }
}
